from django.db import models

class Post(models.Model):
    Name = models.CharField(max_length=200, default = " ")
    year_choice = (("2016","2016 - Omega"), ("2017","2017 - Tarung"), ("2018","2018 - Quanta"), ("2019","2019 - Maung"))
    Year = models.TextField(choices = year_choice, default = " ")
    Hobby = models.TextField(max_length=200, blank=True, default = " ")
    Favourite_Food = models.TextField(max_length=200, blank=True, default = " ")
    Favourite_Drink = models.TextField(max_length=200, blank=True, default = " ")

    def __str__(self):
        return "{}".format(self.Name)