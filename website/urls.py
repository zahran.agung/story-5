from django.urls import include, path
from website import views as profile_views

urlpatterns = [
    path('',profile_views.home , name='home'),
    path('home/',profile_views.home , name='home'),
    path('about/',profile_views.about , name='about'),
    path('resume/',profile_views.resume , name='resume'),
    path('forms/',profile_views.forms , name='forms'),
    path('friends/',profile_views.friends , name='friends'),
]
