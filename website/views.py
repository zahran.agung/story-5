from django.shortcuts import render
from django.http import HttpResponse
from .models import Post
from .forms import FriendForm

# Create your views here.

def home(request):
    return render(request, 'home.html')

def about(request):
    return render (request, 'about.html')

def resume(request):
    return render (request, 'resume.html')

def forms(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
            return render (request, 'forms.html')
    else:
        form = FriendForm()
        return render (request, 'forms.html', {'form' : form})

def friends(request):
    post = Post.objects.all()
    context = {
        'Posts' : post,
    }
    return render (request, 'friends.html', context)